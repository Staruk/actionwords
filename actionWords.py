import xmltodict, glob, re
from nltk.stem.porter import *
from nltk.stem import WordNetLemmatizer
from nltk import pos_tag #,word_tokenize #python2
from nltk.tokenize import word_tokenize #python3
from collections import defaultdict
import os, time, pickle

from nltk.parse import stanford


# make this based on user name instead of something manually updated
user = os.environ['USER']
if user=='artemis':
  os.environ['STANFORD_PARSER'] = '/usr/local/stanford-parser-full-2015-04-20/'
  os.environ['STANFORD_MODELS'] = '/usr/local/stanford-parser-full-2015-04-20/'
  english_model_path = '/usr/local/stanford-parser-full-2015-04-20/edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz'
elif user=='jango':
  os.environ['STANFORD_PARSER'] = '/usr/local/parser/stanford-parser-full-2015-12-09/'
  os.environ['STANFORD_MODELS'] = '/usr/local/parser/stanford-parser-full-2015-12-09/'
  english_model_path = '/usr/local/parser/stanford-parser-full-2015-12-09/edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz'
else:
  print "Unknown path to Stanford parser and models. Please see code. (EAS)"
# NOTE: Parsing a sentence results in an ITERATOR.
# So, you need to keep track of the output while iterating, or you will lose it! D:

##############################################################


# Get trees for all sentences in: contexts, utterances
# Takes a name for filein
# If you don't want a fileout, print to console
# If you do but don't pick an output name, it will mimic the input
# Or you can specify a name instead (overloaded variable)
def getTrees(text, filein, fileout=None, outpath='./output/'):
  english_path_model = '/usr/local/parser/stanford-parser-full-2015-12-09/edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz'
  parser = stanford.StanfordParser(model_path = english_path_model)
  # EAS: Hack solution for problematic jars! Make more graceful!
  jars = [_ for _ in parser._classpath]
  jars.append('/usr/local/parser/stanford-parser-full-2015-12-09/slf4j-api.jar')
  jars.append('/usr/local/parser/stanford-parser-full-2015-12-09/slf4j-api.jar')
  parser._classpath = jars

  data = loadFile(filein)
#  (speakers, contexts, utterances) = getDialogueFromData(data)
  verbs = None
  if text=='utterances':
    (speakers, utterances) = getDialogueFromData(data, text)
    verbs = findVerbsInUtterances(speakers, utterances, stopwords, lemmas=True)
  elif text=='contexts':
    (contexts) = getDialogueFromData(data, text)
    verbs = findVerbsInContexts(contexts, stopwords, lemmas=True)
  else:
    print "Not a valid kind of text"

  all_sentences = []
  for elt in verbs:
    if elt[-1] not in all_sentences:
      all_sentences.append(elt[-1])

  start_time = time.time()
  if fileout==None:
    trees = []
    for sentence in all_sentences:
      tree = parser.raw_parse(sentence)
      trees.append(tree)
      print "Finished processing the following: ", sentence
    print "%s\t--- %s seconds ---" %(filein, time.time()-start_time)
    return trees

  else:
    if not type(fileout)==str:
      fileout = filein[ filein.rindex('/')+1 : filein.rindex('.xml') ] + '.out.'
    with open(outpath + fileout + str(len(all_sentences)), 'wb') as fout:
      # the number at the end is the number of sentences needed to store data structures. when reading in the pickle, do:
            # with open(filein, 'rb') as fin:
              # for _ in range(THAT_NUMBER):
                # all_data.append(pickle.load(fin))
      for sentence in all_sentences:
        tree = parser.raw_parse(sentence)
        for elt in tree: # needed to deal with this program's data structure
          pickle.dump(elt, fout)
    print "%s\t--- %s seconds ---" %(filein, time.time()-start_time)


##############################################################

stopwords = set(['am', 'is', 'are', 'was', 'were', 'be', 'been', 'being',
                 'have', 'has', 'had', 'having', 'do', 'does', 'did',
                 'doing', 's', 't', 'can', 'will', 'don', 'should', 'ai']) # remove later



"""
def processFile(movie, script_path = '/home/artemis/Desktop/MICA/scripts/', gender_path = '/home/artemis/Desktop/MICA/speakersWithCharacterInfo/'):
#  script_path = '/home/artemis/Desktop/MICA/scripts/' # remove later
#  gender_path = '/home/artemis/Desktop/MICA/speakersWithCharacterInfo/' # remove later
  # handmade set of stopwords from NLTK's english stopword list, verbs only, + 'ai' since it's how NLTK processes "ain't", which is a form of "to be"
  stopwords = set(['am', 'is', 'are', 'was', 'were', 'be', 'been', 'being',
                   'have', 'has', 'had', 'having', 'do', 'does', 'did',
                   'doing', 's', 't', 'can', 'will', 'don', 'should', 'ai']) # remove later
  xml_filein = script_path + movie + ".xml"
  gender_filein = gender_path + movie + "_xml.txt"

  data = actionWords.loadFile(xml_filein)
  (speakers, contexts, utterances) = actionWords.getTextFromData(data)
  verb_list = actionWords.findVerbsInUtterances(speakers, utterances, stopwords, stems=False, lemmas=True)
  gender_dictionary = actionWords.getGenderDictionary(gender_filein)
  (women_dict, men_dict, other_dict) = actionWords.countVerbsByGender(verb_list, gender_dictionary)
  return (women_dict, men_dict, other_dict)
"""

# loads XML file
def loadFile(filein):
  with open(filein) as fin:
    data = xmltodict.parse(fin.read())
  return data

# takes XML script data
# returns the speakers, contexts, and utterances that occur, in order
def getDialogueFromData(data, text):
  dialogue = data['movie']['dialogue']
  speakers = []
  contexts = []
  utterances = []
  if text=='utterances':
    for dia in dialogue:
      speaker = dia['speaker']
      utterance = dia['utterance']
      speakers.append(speaker)
      utterances.append(utterance)
    return ((speakers, utterances))
  elif text=='contexts':
    for dia in dialogue:
      context = dia['context']
      contexts.append(context)
    return ((contexts))
  elif text=='both':
    for dia in dialogue:
      utterances.append(dia['utterance'])
      contexts.append(dia['context'])
      speakers.append(dia['speaker'])
    return ((speakers, contexts, utterances))
  else:
    print "Problem with 'text' input type; not 'utterances', 'contexts', or 'both'" 
    return ((speakers, contexts, utterances))

# returns a list of (speaker, verb, tag, utterance) whenever verb occurs in dialogue
# can choose to stem or lemmatize verbs
# lemmatizing takes priority over stemming, if you ask to do both
def findVerbsInUtterances(speakers, utterances, stopwords, stems=False, lemmas=True):
  if stems: stemmer = PorterStemmer()
  if lemmas: lemmatizer = WordNetLemmatizer()
  verbs = []
  for i in range(len(utterances)):
    for j in range(len(utterances[i])):
      for utterance in utterances[i][j].split('. '): # if multiple sentences in an utterance, process them independently
        if not ' ' in utterance: # if there's only one word
          pass
        else:
          speaker = speakers[i][j]    
          tokens = [word.lower() for word in word_tokenize(utterance)]
          tags = pos_tag(tokens)
          #TODO: parser.tagged_parse(tags)
          # only if there is a V-tag that's not in stopwords
          for word,tag in tags:
            if tag[0]=="V" and not word[0]=="'" and word not in stopwords:
              cleanedWord = ""
              if stems: cleanedWord = stemmer.stem(word)
              if lemmas: cleanedWord = lemmatizer.lemmatize(word, pos='v')
              # might want to output the original word that was tagged as well
              verbs.append((speaker,cleanedWord,tag,utterance))
  return verbs

# returns a list of (speaker, verb, tag, utterance) whenever verb occurs in dialogue
# can choose to stem or lemmatize verbs
# lemmatizing takes priority over stemming, if you ask to do both
def findVerbsInContexts(contexts, stopwords, stems=False, lemmas=True):
  if stems: stemmer = PorterStemmer()
  if lemmas: lemmatizer = WordNetLemmatizer()
  verbs = []
  for _ in contexts:
    for sentences in _:
      if sentences: # if it's not just a "None"
        for sentence in sentences.split('. '): # if multiple sentences in this line of context, parse them individually
          if not ' ' in sentence or sentence.isupper(): # if there's only one word, or the entire "sentence" is actually just a camera instruction / POV (which is indicated by all caps)
            pass
          else:
            tokens = [word.lower() for word in word_tokenize(sentence)]
            tags = pos_tag(tokens)
            for word,tag in tags:
              if tag[0]=="V" and not word[0]=="'" and word not in stopwords:
                cleanedWord = ""
                if stems: cleanedWord = stemmer.stem(word)
                if lemmas: cleanedWord = lemmatizer.lemmatize(word, pos='v')
                verbs.append((cleanedWord,tag,sentence))
  return verbs


# take path to movie's gender file
# returns dictionary of character genders
def getGenderDictionary(gender_file):
  #character_rex = re.compile(u'(.*)=>.*\|.*\|.*\|.*\| (\w+) \|')
  #alternative_rex = re.compile(u'(.*)=>.*\| (\w+) \|') #if that gender format is not followed, it's this one
  character_rex = re.compile(u'([^\|]+) \| .*\|  (male|female|unknown)  \|.*')
  gender_dictionary = {}
  with open(gender_file, 'r') as fin:
    for line in fin:
      results = re.search(character_rex, line)
      #if not results:
        #results = re.search(alternative_rex, line)
      if not results: print "Non match for rex: ", line
      gender_dictionary[results.groups()[0]] = results.groups()[1]
  return gender_dictionary


# takes tuple of verb info (as per the output in findVerbsInUtterances above)
# and a gender_dictionary as per getGenderDictionary above
# returns count of verbs from women, men, and unknown gender speakers
def countVerbsByGender(verb_tuple, gender_dictionary):
  women = defaultdict(int); men = defaultdict(int); other = defaultdict(int)
  for v in verb_tuple:
    if not v[0] in gender_dictionary.keys():
      print "Cannot find this actor: ", v
    else:
      if gender_dictionary[v[0]] == "female":
        women[v[1]] += 1
      elif gender_dictionary[v[0]] == "male":
        men[v[1]] += 1
      elif gender_dictionary[v[0]] == "unknown":
        other[v[1]] += 1
      else:
        print v[0], gender_dictionary[v[0]]
  return (women, men, other)


# TODO:
#    parse sentences using dependency parser
#    assign actor to verbs
#	* sentence-level ("I")
#	* dialogue-level ("you")
#	* script-level ("he/she/they")
#	* world-level ("IBM")
#    assign genders to actors
#    link verbs and their synonyms using wordnet
#	* wordnet.synsets(word)
#    process contexts, not just dialogues
