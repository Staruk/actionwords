from actionWords import *


stopwords = set(['am', 'is', 'are', 'was', 'were', 'be', 'been', 'being',
                 'have', 'has', 'had', 'having', 'do', 'does', 'did',
                 'doing', 's', 't', 'can', 'will', 'don', 'should', 'ai'])

movie = 'x_men'

data = loadFile('../scripts/' + movie + '.xml')
(speakers, utterances) = getDialogueFromData(data, 'utterances')
verbs = findVerbsInUtterances(speakers, utterances, stopwords)
gender_dictionary = getGenderDictionary('../speakersWithAverageGenderLadenness/' + movie + '_xml.txt')

output = countVerbsByGender(verbs, gender_dictionary)
