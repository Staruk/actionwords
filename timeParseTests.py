from nltk import pos_tag
from nltk.parse import stanford
from nltk.tokenize import word_tokenize
import actionWords, time, glob, os
###############################################################

# 	Conclusion: these trees all favor the raw parse.
#	 Let the Parser do the tagging on its own!!!

###############################################################

# parser with an untagged sentence, no modification
def timeRegularParse(parser, sentence):
  start_time = time.time()
  r = parser.raw_parse(sentence)
  print("--- %s seconds ---" %(time.time()-start_time))
  return r

# parser with a tagged sentence, on-the-fly
def timeParseWithTagging(parser, sentence):
  start_time = time.time()
  tagged_sentence = pos_tag(word_tokenize(sentence)) #[word.lower() for word in word_tokenize(sentence)])
  r = parser.tagged_parse(tagged_sentence)
  print("--- %s seconds ---" %(time.time()-start_time))
  return r

# parser with a tagged sentence, pre-tagged
def timePretaggedParse(parser, tagged_sentence):
  start_time = time.time()
  r = parser.tagged_parse(tagged_sentence)
  print("--- %s seconds ---" %(time.time()-start_time))
  return r

# compare all types of parsing
def compareParses(parser, sentence):
  print(sentence)
  start_time = time.time()
  raw_r = parser.raw_parse(sentence)
  print("Raw Parse:\t\t--- %s seconds ---" %(time.time()-start_time))

  start_time = time.time()
  tagged_sentence = pos_tag([word.lower() for word in word_tokenize(sentence)])
  tagged_r = parser.tagged_parse(tagged_sentence)
  print("Tagged Parse:\t\t--- %s seconds ---" %(time.time()-start_time))

  start_time = time.time()
  pretagged_r = parser.tagged_parse(tagged_sentence)
  print("Pretagged Parse:\t--- %s seconds ---" %(time.time()-start_time))

  print('\n')
  return (raw_r, tagged_r, pretagged_r)

###############################################################

jango = True # using my desktop
artemis = False # using my laptop

if artemis:
  os.environ['STANFORD_PARSER'] = '/usr/local/stanford-parser-full-2015-04-20/'
  os.environ['STANFORD_MODELS'] = '/usr/local/stanford-parser-full-2015-04-20/'
  english_model_path = '/usr/local/stanford-parser-full-2015-04-20/edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz'
elif jango:
  os.environ['STANFORD_PARSER'] = '/usr/local/parser/stanford-parser-full-2015-12-09/'
  os.environ['STANFORD_MODELS'] = '/usr/local/parser/stanford-parser-full-2015-12-09/'
  english_model_path = '/usr/local/parser/stanford-parser-full-2015-12-09/edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz'
else:
  print "Unknown path to Stanford parser and models. Please see code. (EAS)"
parser = stanford.StanfordParser(model_path = english_model_path)

# EAS: Hack solution for problematic jars! Make more graceful!
jars = [_ for _ in parser._classpath]
jars.append('/usr/local/parser/stanford-parser-full-2015-12-09/slf4j-api.jar')
jars.append('/usr/local/parser/stanford-parser-full-2015-12-09/slf4j-api.jar')
parser._classpath = jars

#path_scripts = '/home/artemis/Desktop/MICA/scripts/'
#files = glob.glob(path_scripts + "*.xml")
#filein = files[20]
#data = actionWords.loadFile(filein)
#(speakers, contexts, utterances) = actionWords.getTextFromData(data)

sentences = [u"I start fighting a war I guarantee you'll see something new.",
 u'Nobody besides Jayne is saying that.',
 u"How can it be there's a whole planet called Miranda and none of us knowed that?",
 u"That's right at the edge of the Burnham Quadrant, right?",
 u"But there's nothing about it on the Cortex -- History, Astronomy... it's not in there.",
 u'They just float out there, sending out raiding parties --',
 u"Let's get to the beacon.",
 u"I'm gonna have to glide her in!",
 u'Will that work?',
 u'140 Long as that landing strip is made of fluffy pillows...',
 u'Everybody to the upper decks!',
 u"Tell me you brought 'em this time...",
 u'Reavers rush toward them.',
 u'Move the gorram crates! Come on!',
 u"Wait, Wash -- where's Wash?",
 u'I might...',
 u'You really think any of us are gonna get through this?',
 u'She picked a sweet bung of a time to go helpless on us.',
 u'My one true regret in all this is never being with you.',
 u'I mean to say.']

results = []
trees = [] #trees[i][0,1] is i'th sentence, raw [0] or tagged [1]
for sentence in sentences:
  (raw_r, tagged_r, pretagged_r) = compareParses(parser, sentence)
  results.append((raw_r, tagged_r, pretagged_r))
  resulting_trees = []
  for r in results:
    for elt in r[:2]:
      for tree in elt:
        resulting_trees.append(tree)
  trees.append(resulting_trees)



